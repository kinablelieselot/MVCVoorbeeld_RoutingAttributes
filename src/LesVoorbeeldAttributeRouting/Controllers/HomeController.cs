﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using MyHowest;

// For more information on enabling MVC for empty projects, visit http://go.microsoft.com/fwlink/?LinkID=397860

namespace LesVoorbeeldAttributeRouting.Controllers
{
    public class HomeController : Controller
    {
        private string[] groenten;

        public HomeController()
        {
            groenten = new string[] { "Rode kool", "Bloemkool", "Sla", "Komkommer" };
        }

        public string Hello(string name) => $"Hallo {name}!!!";

        public IActionResult Index()
        {
            ViewBag.message = (DateTime.Now.Hour < 12) ? "Goedemorgen" : "Goedemiddag";
            return View();
        }

        public IActionResult Groenten(string zoekGroente)
        {
            ViewBag.groenten = groenten;
            if (!string.IsNullOrEmpty(zoekGroente))
                    ViewBag.zoekResultaat = $"De groente {zoekGroente} is de {Array.IndexOf(groenten, zoekGroente) +1}e groente in de lijst ";
            return View();
        }


        public IActionResult Student()
        {
            Student howestStudent = new MyHowest.Student()
                { Id=345, Naam="Jason Page", Afstudeergraad=Graad.Voldoende };

            List<Student> studenten = new List<MyHowest.Student>();
            studenten.Add(new MyHowest.Student() { Id=444, Naam="Karen Desmet", Afstudeergraad=Graad.Onderscheiding });
            studenten.Add(howestStudent);

            ViewBag.studenten = studenten;
            return View();
        }

        public IActionResult GroentenJSON()
        {
            return Json(groenten);
        }

    }
}
